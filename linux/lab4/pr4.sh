 #!/bin/bash
set -e
    scriptPath="$(pwd)/$(basename $0)"
    crontaskFile="$(dirname $0)/crontask"
    strWelcome="Выберите необходимое действие с контроллируемыми программами:"
    strCat="Вывести список"
    strAdd="Добавить программу в список"
    strRemove="Удалить программу из списка"
    strExit="Выход"
    declare -a actionList=("$strCat" "$strAdd" "$strRemove" "$strExit")  
    
function help() {
    echo
    echo "  Использование: $scriptName [OPTION]"
    echo "  OPTIONS:"
    echo -e "\t-h\tПоказать данную справку"
    echo -e "\t--checkAlarm [arg1]\t выполнить проверку процесса [arg1]"
    echo
}

function list() {
    if (("${#alarmList[@]}")); 
        then 
            echo  "Список проверяемы задач:"
            for alarm in "${!alarmList[@]}"; do
                echo -n "$(($alarm+1))) "
                echo "${alarmList[$alarm]}" | sed  's|'"\* \* \* export DISPLAY=:0 && $scriptPath --checkAlarm ||g" | sed 's/\([0-9\*]*\) \([0-9\*]*\)/Каждые \2:\1 проверять процесс/'
            done
        else 
            echo "Список проверяемы задач пуст!"
    fi
}

function addAlarm() {
    echo -e "Создание проверки:\nВведите время срабатывания в формате \"hh:mm\""
    read time; 
    while ((!$(echo "$time" | grep "^\(\(\([01]\?[0-9]\|[2][0-3]\):[0-5][0-9]\)\|\(\*:\*\)\)\$" | wc -l))); do 
        echo -e "Неверное значение!\nВведите время срабатывания в формате \"hh:mm\""
        read time; 
    done
    time=$(echo $time | sed 's/\([0-9\*]*\):\([0-9\*]*\)/\2 \1/')   
    echo "$time"
    echo "Введите имя процесса:"
    read -e procName
    echo "$time * * * export DISPLAY=:0 && $scriptPath --checkAlarm $procName"
    echo "$time * * * export DISPLAY=:0 && $scriptPath --checkAlarm $procName" >> $crontaskFile \
    && crontab $crontaskFile  \
    && echo "Проверка успешно создана!" \
        || echo "Ошибка создания проверки!"
}

function removeAlarm() {
    if (("${#alarmList[@]}")); 
        then 
            echo -n  "Выберите проверку для удаления! " && list
            echo -n "#? "
            read REPLY
            sed -i "${REPLY}d" $crontaskFile && crontab $crontaskFile && echo "Проверка №$REPLY был удалён" || echo "Ошибка удаления проверки!";
        else 
            echo "Список проверок пуст"
    fi
}

function showMenu() {
    touch $crontaskFile && mapfile -t alarmList < $crontaskFile
    echo "$strWelcome"
    select CHOICE in "${actionList[@]}";
    do
    case $CHOICE in
            "$strCat") list;;
            "$strAdd") addAlarm;;
            "$strRemove") removeAlarm;;
            "$strExit") echo "Выход"; exit 0;;
            "$QUIT") echo "Выход"; exit 0;;
    esac
    break;
    done
}

# Если скрипт запущен без аргументов, продолжаем работу со стандартными параметрами.
if (( $# == 0 )); then
    while(true); do 
        showMenu; 
    done;
else
    while [[ $# -gt 0 ]] ; do
        case $1 in
            '-h')             
                help
                exit 0;;
                
            '--checkAlarm')          
                shift
                if (($(ps -e | grep "[0-9][0-9]:[0-9][0-9]:[0-9][0-9] $1\$" | wc -l))); then
                    echo "Процесс $1 работает!"
                else
                    echo -e "Процесс $1 не работает!\nЗапускаю..."
                    "$1" &
                fi
                exit 0;;
                
           -*) 
                help $1
                exit 1;;
            *) help $1
                exit 1;;
        esac
        shift
done
fi

