#!/bin/bash
set -e

scriptName=$(basename $0)
imode=0
pmode=0
dirName="dir/c"
subName="subDir/c"
fileName="file/c"
dirCount=5
subCount=10
fileCount=20
toPath=$(pwd)
flag=0

function help() {
if [ $# == 0 ]; then
    echo -en "\033[1mВеликий создатель папок и\033[22m ̶м̶а̶м̶о̶к \033[1mфайлов\033[22m"
    else
    echo -e "\033[1m\033[7mВНИМАНИЕ, ОШИБКА!\033[27m\033[22m\n  неизвестный флаг \033[1m\"$1\"\033[22m"
fi
    echo
    echo "  Использование: $scriptName [OPTION [arg]] [path=\$pwd]"
    echo "  OPTIONS:"
    echo -e "\t-h\tПоказать данную справку"
    echo -e "\t-p\tСоздавать отсутствующие в заданом пути папки"
    echo -e "\t--dirCount [count=$dirCount]\tКоличество папок"
    echo -e "\t--subCount [count=$subCount]\tКоличество вложенных папок"
    echo -e "\t--fileCount [count=$fileCount]\tКоличество файлов"  
    echo  
    echo "  ШАБЛОНЫ ИМЁН:"
    echo -e "\tНа место последовательности \"/c\" в шаблоне имени\n\tбудет вставлен порядковый номер файла"
    echo -e "\tт.о.:\n\t\t\"someName #/c\" <=> \"someName #1\""
    echo
    echo -e "\t--dirName [name=\"$dirName\"]\tШаблон имени папок"
    echo -e "\t--subName [name=\"$subName\"]\tШаблон имени вложенных папок"
    echo -e "\t--fileName [name=\"$fileName\"]\tШаблон имени файлов"
    echo
}

function getName(){
    local answ=$1
    if ((!$(echo "$1" | grep "\/c" | wc -l))); then
        answ="$1/c"
    fi
    echo "$answ" | sed "s/\/c/$2/"
}

function doDirs() {
    for i in $(seq 1 $dirCount); do
        dirPath="$toPath/$(getName $dirName $i)"
        if (( "$pmode" )); then
            mkdir -p "$dirPath"
            else
            mkdir "$dirPath"
        fi
        for j in $(seq 1 $subCount); do
            subPath="$dirPath/$(getName $subName $j)"
            mkdir "$subPath"    #реализовать шаблон имён
            for k in $(seq 1 $fileCount); do
                filePath="$subPath/$(getName $fileName $k)"
                touch "$filePath"
            done
        done        
    done
    exit 0
}

# Если скрипт запущен без аргументов, продолжаем работу со стандартными параметрами.
if [ $# == 0 ]; then
    doDirs
fi

# Разбор параметров.
while [[ $# -gt 0 ]] ; do
        case $1 in
            '-h')             
                help
                exit 0;;
                
            '-p')          
                echo "pmode activated"
                pmode=1;;
                
            '--dirName')             
                shift
                dirName=$1;;
                
            '----subName')             
                shift
                subName=$1;;
                
            '--fileName')             
                shift
                fileName=$1;;
                
            '--dirCount')             
                shift
                dirCount=$1;;
                
            '--subCount')             
                shift
                subCount=$1;;
                
            '--fileCount')             
                shift
                fileCount=$1;;
                
           -*) 
                help $1
                exit 1;;
            *) toPath=$1;;
        esac
        shift
done

#Работаем!!
doDirs;    
