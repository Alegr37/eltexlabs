 
 #!/bin/bash
    #crontaskFile="$(dirname $0)/crontask"
    strWelcome="Выберите необходимый раздел действий с git репозиторием"
    strStatus="Статус"
    strRepoWelcome="Выберите необходимое действие с репозиторием"
    strBranchesWelcome="Выберите необходимое действие с ветками"
    strFilesWelcome="Выберите необходимое действие с файлами"
    strCommitsWelcome="Выберите необходимое действие с коммитами"
    strRepo="Репозитории"
    strBranches="Ветки"
    strFiles="Файлы"
    strAdd="Добавить к индексации"
    strCommits="Коммиты"
    strPush="Выгрузить на сервер"
    strPull="Загрузить с сервера"
    strShow="Отобразить"
    strCreate="Добавить"
    strRename="Переименовать"
    strChange="Изменить"
    strRemove="Убрать"
    strDelete="Удалить"
    strBack="Назад"
    
    declare -a actionList=("$strBranches" "$strFiles" "$strCommits" "$strPush"  "$strPull" "$strExit")  
    
function gitStatus() {
    whiptail --title "git status" --msgbox "$(git status)" 26 78
}

function selectFiles() {
    IFS=$(echo -en "\n\b")
    scriptPath="$(pwd)"
    #filesInDir=$(echo -n $(ls -a "$scriptPath" | sed 's/\([^\n]*\)/ "\1" "Выбрать1 Выбрать2"/'))
    for someFile in *; do
    if [ -d "$someFile" ]; 
        then 
            if [[ "$someFile" == "." ]]
                then
                    someFile="\"$someFile\" \"Выбрать текущую директорию\"";
                else
                    someFile="\"$someFile\" \"перейти в папку\""; 
            fi
        else 
            someFile="\"$someFile\" \"выбрать файл\"";  
    fi;
    filesInDir="$filesInDir ""$someFile"
    done
    ass="whiptail --title \"$scriptPath\" --menu \"\nСделайте выбор\" 26 78 15 $filesInDir  3>&1 1>&2 2>&3"
    choice=$(eval "$ass")
    if [[ "$choice" != "." && -d "$choice" ]]
        then
        cd "$scriptPath/$choice"
        selectFiles 
        else
        echo "$scriptPath/$choice"
    fi
}

function branchesFunc() {
        choice=$(whiptail --title "$strBranchesWelcome" --menu "\nСделайте выбор" 26 78 15 \
        "$strShow" "Отобразить существующие ветки" \
        "$strCreate" "Создание ветки" \
        "$strChange" "Изменение активной ветки"  \
        "$strDelete" "Удаление ветки"  \
        "$strBack" ""  3>&1 1>&2 2>&3)

    if [[ $? = 0 ]]; then
    case $choice in
            "$strShow") whiptail --title "git branch" --msgbox "$(git branch)" 26 78 && branchesFunc;;
            "$strCreate") branchName=$(whiptail --inputbox "Введите имя ветки:" 8 78 "" --title "Создание новой ветки" 3>&1 1>&2 2>&3) && git branch "$branchName" && branchesFunc;;
            "$strChange") branchName=$(whiptail --inputbox "Введите имя ветки:" 8 78 "" --title "Изменение активной ветки" 3>&1 1>&2 2>&3) && git checkout "$branchName" && branchesFunc;;
            "$strDelete") branchName=$(whiptail --inputbox "Введите имя ветки:" 8 78 "" --title "Удаление ветки" 3>&1 1>&2 2>&3) && git branch -d "$branchName" && branchesFunc;;
            "$strBack") showMenu;;
    esac
    fi
    showMenu
}

function filesFunc() {
        choice=$(whiptail --title "$strFilesWelcome" --menu "\nСделайте выбор" 26 78 15 \
        "$strShow" "Отобразить отслеживаемые файлы" \
        "$strCreate" "Добавление файла к индексации." \
        "$strRename" "Переименование файла"  \
        "$strRemove" "Убрать файл из коммита"  \
        "$strDelete" "Удаление файла из репозитория"  \
        "$strBack" ""  3>&1 1>&2 2>&3)
    if [[ $? = 0 ]]; then
        case $choice in
                "$strShow") whiptail --title "git ls-files" --msgbox "$(git ls-files)" 26 78 && filesFunc;;
                "$strCreate") addedFile=$(selectFiles | sed "s|$(pwd)/||") && git add "$addedFile" && gitStatus;;
                "$strRename")  renamedFile=$(selectFiles | sed "s|$(pwd)/||") && newName=$(whiptail --inputbox "Введите новое имя:" 8 78 "$renamedFile" --title "Переименовывание" 3>&1 1>&2 2>&3) && git mv "$renamedFile" "$newName"  && gitStatus;;                                        
                "$strRemove") removedFile=$(selectFiles | sed "s|$(pwd)/||") && git reset HEAD "$removedFile" && gitStatus;;                                   
                "$strDelete") deletedFile=$(selectFiles | sed "s|$(pwd)/||") && git rm --cached "$deletedFile" && gitStatus;;
                "$strBack") showMenu;;
        esac
    fi
    showMenu
}

function commitsFunc() {
    choice=$(whiptail --title "$strCommitsWelcome" --menu "\nСделайте выбор" 26 78 15 \
        "$strShow" "Отобразить историю коммитов." \
        "$strCreate" "Сделать коммит." \
        "$strRename" "Изменить комментарий к комиту."  \
        "$strDelete" "Отмена коммита."  \
        "$strBack" ""  3>&1 1>&2 2>&3)

    if [[ $? = 0 ]]; then
        case $choice in
                "$strShow") whiptail --title "Commit history" --msgbox "$(git log --pretty=format:"%h %s" --graph)" 26 78;;        
                "$strCreate") commitName=$(whiptail --inputbox "Введите комментарий к коммиту:" 8 78 "" --title "Создание коммита" 3>&1 1>&2 2>&3) && git commit -m "$commitName";;
                "$strRename") commitName=$(whiptail --inputbox "Введите новый комментарий к коммиту:" 8 78 "" --title "Редактирование коммита" 3>&1 1>&2 2>&3) && echo "$commitName" | git commit --amend;;
                "$strDelete") commitName=$(whiptail --inputbox "Введите хэш коммита для отката:" 8 78 "HEAD" --title "Редактирование коммита" 3>&1 1>&2 2>&3) && git revert "$commitName" --no-edit;;
                "$strBack") showMenu;;
        esac
    fi
    showMenu
}

function repoFunc() {
    choice=$(whiptail --title "$strRepoWelcome" --menu "\nСделайте выбор" 26 78 15 \
        "$strCreate" "Инициация репозитория." \
        "$strRename" "Переименование репозитория."  \
        "$strDelete" "Удаление репозитория."  \
        "$strBack" ""  3>&1 1>&2 2>&3)

    case $choice in
            "$strCreate") git init;;
            "$strRename") filesFunc;;
            "$strDelete") commitsFunc;;
            "$strBack") showMenu;;
    esac
}

function pushFunc() {
    choice=$(whiptail --title "$strRepoWelcome" --menu "\nСделайте выбор" 26 78 15 \
        "$strCreate" "Инициация репозитория." \
        "$strRename" "Переименование репозитория."  \
        "$strDelete" "Удаление репозитория."  \
        "$strBack" ""  3>&1 1>&2 2>&3)

    case $choice in
            "$strCreate") git init;;
            "$strRename") filesFunc;;
            "$strDelete") commitsFunc;;
            "$strBack") showMenu;;
    esac
}

function pullFunc() {
    choice=$(whiptail --title "$strRepoWelcome" --menu "\nСделайте выбор" 26 78 15 \
        "$strCreate" "Инициация репозитория." \
        "$strRename" "Переименование репозитория."  \
        "$strDelete" "Удаление репозитория."  \
        "$strBack" ""  3>&1 1>&2 2>&3)

    case $choice in
            "$strCreate") git init;;
            "$strRename") filesFunc;;
            "$strDelete") commitsFunc;;
            "$strBack") showMenu;;
    esac
}
#declare -a actionList=("$strBranches" "$strFiles" "$strCommits" "$strPush"  "$strPull" "$strExit")  
function showMenu() {
    choice=$(whiptail --title "$strWelcome" --menu "\nСделайте выбор" 26 78 15 \
        "$strStatus" "Отобразить статус git репозитория" \
        "$strRepo" "Создание/Изменение/Удаление репозитория." \
        "$strBranches" "Создание/Изменение/Удаление веток." \
        "$strFiles" "Создание/Изменение/Удаление файлов."  \
        "$strCommits" "Создание/Изменение/Удаление коммитов."  \
        "$strPush" "Выгрузить изменения на сервер."  \
        "$strPull" "Загрузить изменения с сервера." 3>&1 1>&2 2>&3)

    case $choice in
            "$strStatus") gitStatus; showMenu;;
            "$strRepo") repoFunc;;
            "$strBranches") branchesFunc;;
            "$strFiles") filesFunc;;
            "$strCommits") commitsFunc;;
            "$strPush") serverName=$(whiptail --inputbox "Введите название удаленного сервера и ветви:" 8 78 "origin master" --title "git push" 3>&1 1>&2 2>&3) && git push "$serverName";;
            "$strPull") serverName=$(whiptail --inputbox "Введите название удаленного сервера и ветви:" 8 78 "origin master" --title "git pull" 3>&1 1>&2 2>&3) && git pull "$serverName";;
            "$QUIT") echo "Выход"; exit 0;;
    esac
}

# Если скрипт запущен без аргументов, продолжаем работу со стандартными параметрами.

showMenu; 
