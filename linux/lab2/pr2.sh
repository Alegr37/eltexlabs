#!/bin/bash
set -e

strWelcome="Выберите необходимое действие с будильником:"
strCat="Вывести список"
strCreate="Создать"
strRemove="Удалить"
strExit="Выход"
declare -a actionList=("$strCat" "$strCreate" "$strRemove" "$strExit")                
crontaskFile="$(dirname $0)/crontask"

function list() {
    if (("${#alarmList[@]}")); 
        then 
            echo  "Список будильников:"
            for alarm in "${!alarmList[@]}"; do
                echo -n "$(($alarm+1))) "
                echo "${alarmList[$alarm]}" | sed  's|'"\* \* \* export DISPLAY=:0 && cd $(pwd) && /bin/mpv |$(pwd)/|g" | sed 's/\([0-9]*\) \([0-9]*\)/В \2:\1 проиграть файл/'
            done
        else 
            echo "Список будильников пуст!"
    fi
}

function createAlarm() {
    echo -e "Создание будильника:\nВведите время срабатывания в формате \"hh:mm\""
    read time; 
    while ((!$(echo "$time" | grep "^\([01]\?[0-9]\|[2][0-3]\):[0-5][0-9]\$" | wc -l))); do 
        echo -e "Неверное значение!\nВведите время срабатывания в формате \"hh:mm\""
        read time; 
    done
    hhMM=($(echo "$time" | tr ':' ' '))
    echo "Введите имя файла:"
    read -e fileName
    echo "${hhMM[1]} ${hhMM[0]} * * * export DISPLAY=:0 && cd $(pwd) && /bin/mpv $fileName" >> $crontaskFile \
    && crontab $crontaskFile  \
    && echo "Будильник успешно создан!" \
        || echo "Ошибка создания будильника!"
}

function removeAlarm() {
    if (("${#alarmList[@]}")); 
        then 
            echo -n  "Выберите будильник для удаления! " && list
            echo -n "#? "
            read REPLY
            sed -i "${REPLY}d" $crontaskFile && crontab $crontaskFile && echo "Будильник №$REPLY был удалён" || echo "Ошибка удаления будильника!";
        else 
            echo "Список будильников пуст"
    fi
}

function showMenu() {
    touch $crontaskFile && mapfile -t alarmList < $crontaskFile
    echo "$strWelcome"
    select CHOICE in "${actionList[@]}";
    do
    case $CHOICE in
            "$strCat") list;;
            "$strCreate") createAlarm;;
            "$strRemove") removeAlarm;;
            "$strExit") echo "Выход"; exit 0;;
            "$QUIT") echo "Выход"; exit 0;;
    esac
    break;
    done
}

while(true); do 
    showMenu; 
done;
