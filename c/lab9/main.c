#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <signal.h>

/*
    Голодные игры. Родительский процесс создает указанное количество дочерних, фиксируя их идентификаторы (например, в файле). Каждый дочерний процесс удаляет из списка
    произвольный идентификатор, если он не равен его собственному и отправляет системный запрос на уничтожение другого процесса, чей идентификатор он выбрал. Игра прекращается.
    когда останется один участник, родительский процесс сообщает идентификатор победителя.
*/

#define true 1
#define false 0
#define procCount 40
#define PERM 0666

int shmId;
int semId;
key_t key;

struct Players{
    pid_t lKiller; // последний убийца
    pid_t allPlayers[procCount]; // Массив процессов
};

/* Ожидание единственного выжившего клиента (Операции с семафорами атомарны => выполнение обоих гарантированно) */
static struct sembuf proc_wait_single[2] = {
    {1, -1, 0},
    {1, 0, 0}
};
/* Уведомление о старте дочернего процесса */
static struct sembuf proc_start = {
    1, 1, SEM_UNDO
};
/* Блокирование разделяемой памяти */
static struct sembuf mem_lock[2] = {
    {0, 0, 0},
    {0, 1, SEM_UNDO}
};
/* Освобождение ресурса */
static struct sembuf mem_unlock = {
    0, -1, 0
};

// Функции работы с семафорами
void lockMemory(){
    if (semop(semId, &mem_lock[0], 2) < 0) {
        printf("Err: mem_lock sem\n");
        exit(1);
    }
}
struct Players* attachMemory(){
    struct Players* mpPlayers;
    /* Присоединим память */
    if ((mpPlayers = (struct Players*)shmat(shmId, 0, 0)) < 0) {
        printf("Ошибка присоединения\n");
        exit(1);
    }
    return mpPlayers;
}
void detachMemory(struct Players* pPlayers){
    /* Отключимся от области */
    if (shmdt(pPlayers) < 0) {
     printf("Ошибка отключения\n");
     exit(1);
    }
}
void unlockMemory(){
    if (semop(semId, &mem_unlock, 1) < 0) {
        printf("Err: mem_unlock sem\n");
        exit(1);
    }
}

void startProcess(){
    if (semop(semId, &proc_start, 1) < 0) {
        printf("Err: proc_start sem\n");
        exit(1);
    }
}

void waitForSingleProcess(){
    if (semop(semId, &proc_wait_single[0], 2) < 0) {
        printf("Err: proc_start sem\n");
        exit(1);
    }
}

int main(){
    struct Players* pPlayers;
    /* Получим ключ, Один и тот же ключ можно использовать как
       для семафора, так и для разделяемой памяти */

    if ((key = ftok(".", 'A')) < 0) {
        printf("Невозможно получить ключ\n");
        exit(1);
    }

    /* Создадим область разделяемой памяти */
    if ((shmId = shmget(key, sizeof(struct Players),
                        PERM | IPC_CREAT)) < 0) {
        printf("Невозможно создать область\n");
        exit(1);
    }

    /* Создадим группу из двух семафоров:
        Первый семафор - для синхронизации работы
        с разделяемой памятью. Второй семафор -
        для синхронизации выполнения процессов */
    if ((semId = semget(key, 2, PERM | IPC_CREAT)) < 0) {
        printf("Невозможно создать семафор\n");
        exit(1);
    }
    
    // Блокировка для заполнения сервером pPlayers
    lockMemory();
    pPlayers = attachMemory();
    pPlayers->lKiller = 0;
    for(int i = 0; i < procCount; ++i){
        pid_t pid = fork();
        if(pid == 0){
            startProcess();
            pid_t currentPid = getpid();
            //  обновление ссылки на shm
            pPlayers = attachMemory();
            srand(time(NULL) + currentPid);
            while(true){
                lockMemory();
                int selectedProc = rand()%procCount;
                while(pPlayers->allPlayers[selectedProc] == 0) selectedProc = (selectedProc+1)%procCount;
                if(pPlayers->allPlayers[selectedProc] != currentPid) {
                    if(kill(pPlayers->allPlayers[selectedProc], 9) != -1){
                        printf("%d --> %d\n", currentPid, pPlayers->allPlayers[selectedProc]);
                        pPlayers->allPlayers[selectedProc] = 0;
                        pPlayers->lKiller = currentPid;
                    }
                    else printf("%d --x %d\n", currentPid, pPlayers->allPlayers[selectedProc]);

                }
                else printf("%d trying to suicide\n", currentPid);

                unlockMemory();
                sleep(1); // sleep after kill
            }
        }
        else{
            pPlayers->allPlayers[i] = pid;
        }
    }
    // завершение заполнения pPlayers
    unlockMemory();
    waitForSingleProcess();
    printf("Proc %d win the game!\nCongrulations!!\nNow we killing him :3\n", pPlayers->lKiller);
    kill(pPlayers->lKiller, 9);
    detachMemory(pPlayers);
    /* Удалим созданные объекты IPC */
    if (shmctl(shmId, IPC_RMID, 0) < 0) {
     printf("Невозможно удалить область\n");
     exit(1);
    }
    if (semctl(semId, 0, IPC_RMID) < 0) {
     printf("Невозможно удалить семафор\n");
     exit(1);
    }
    exit(0);
}
