#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#define TokenNotFound -1

typedef enum TR_actions_e{
	get,
	set,
	TR_actions_count,
}TR_actions;

typedef enum ValueType_e{
	IntType,
	CharType,
	StringType,
}ValueType;


typedef struct TypeAndSize_e{
	int type;
	size_t size;
}TypeAndSize;

TypeAndSize types_and_sizes[] = {
	{IntType, sizeof(int)},
	{CharType, sizeof(char)},
	{StringType, 256}, //TODO: fix this constant
	{TokenNotFound, 0}
};

size_t get_type_size(ValueType type){
	TypeAndSize *locTAS = types_and_sizes;
	while(locTAS->type != TokenNotFound && locTAS->type != type) ++locTAS;
	return locTAS->size;
}

typedef struct Token_s{
	const int Id;
	const char *name;
}Token;

typedef enum IGD_e{
	Igd,
	Summary,
	Ass,
}IGD;

Token tokenList[] = {
	{Igd , "Igd"},
	{Summary , "Summary"},
	{Ass , "Ass"},
};

int get_token_id(const Token *tokenList, const char *name){
	while(tokenList && strcmp(tokenList->name, name)) ++tokenList;
	return tokenList->Id;
}

const char *get_token_name(const Token *tokenList, int Id){
	while(tokenList && tokenList->Id != Id) ++tokenList;
	return tokenList->name;
}

typedef struct Tree_s{
	long Id;
	Token *associatedTokens;
	void *value;
	int value_type;
	size_t value_size;
	short value_changed;

	int (*get)(struct Tree_s *tree, char *output);
	int (*set)(struct Tree_s *tree, const char *input);

	void *parent;
	void *next;
	void *child;

} Tree;

void print_tree_branch(Tree *tree){
	if(tree->parent) print_tree_branch(tree->parent);
	// else printf("%s.", get_token_name(tokenList, tree->Id));
	printf("%s.", get_token_name(tree->associatedTokens, tree->Id));
}

int dummy_get(Tree *tree, char *output){
	printf("%s: ", __FUNCTION__);
	print_tree_branch(tree);
	switch(tree->value_type){
		case StringType:
			printf("\"%s\" [StringType]\n", (char*)tree->value);
			break;
		case IntType:
			printf("\"%d\" [IntType]\n", *(int*)tree->value);
			break;
		case CharType:
			printf("\"%c\" [CharType]\n", *(char*)tree->value);
			break;
	}
	tree->value_changed = 0;
	return 0;
}

int dummy_set(Tree *tree, const char *input){
	int intVal;
	printf("%s: ", __FUNCTION__);
	switch(tree->value_type){
		case StringType:
			if(strncmp(input, (char*)tree->value, tree->value_size)){
				strncpy((char*)tree->value, input, tree->value_size);
				tree->value_changed = 1;
			}
			printf("[StringType] \"%s\"", (char*)tree->value);
			break;
		case IntType:
			sscanf(input, "%d", &intVal);
			if(intVal != *(int*)tree->value) {
				*(int*)tree->value = intVal;
				tree->value_changed = 1;
			}
			printf("[IntType] \"%d\"\n", *(int*)tree->value);
			break;
		case CharType:
			printf("[CharType] \"%c\"\n", *(char*)tree->value);
			break;
	}
	printf("(%s)\n", (tree->value_changed) ? "changed" : "not changed");
	return 0;
}

int set_tree_value(Tree* tree, const char *input){
	return tree->set(tree, input);
}

int get_tree_value(Tree* tree, char *output){
	return tree->get(tree, output);
}

Tree *create_tree(long Id, const void *value, ValueType value_type, size_t value_size){
	Tree* newTree = NULL;
	if(!(newTree = malloc(sizeof(Tree))))
		goto ret_null;
	memset(newTree, 0, sizeof(Tree));
	newTree->Id = Id;
	newTree->value_type = value_type;
	if(!value_size){
		if(!(newTree->value_size = get_type_size(value_type)))
			goto ret_null;
	}
	else{
		newTree->value_size = value_size;
	}
	if(!(newTree->value = malloc(newTree->value_size))){
		free(newTree);
		return NULL;
	}
	if(value){
		if(newTree->value_type != StringType)
			memcpy(newTree->value, value, newTree->value_size);
		else
			strncpy((char*)newTree->value, (char*)value, newTree->value_size);
	}
	else
		memset(newTree->value, 0, newTree->value_size);
	return newTree;

	ret_null:
		if(newTree){
			free(newTree);
		}
		return NULL;
}

void delete_tree(Tree *tree){
	if(tree->parent) {
		Tree* parent = tree->parent;
		if(parent->child == (void*)tree){
			if(tree->next)
				parent->child = tree->next;
			else
				parent->child = NULL;
		}
	}
	Tree *child = tree->child;
	if(child){
		if(child->next) delete_tree(child->next);
		delete_tree(child);
	}
	free(tree->value);
	free(tree);
}

Tree *get_tree_entry(Tree *tree, long Id){
	while(tree->Id != Id){
		if(!tree->next)
			return NULL;
		tree = tree->next;
	}
	return tree;
}

Tree *get_tree_child(Tree *tree, long Id){
	if(!tree->child || 
			!(tree = get_tree_entry(tree->child, Id)))
			return NULL;
	return tree;
}

Tree *get_tree_recursive(Tree *tree, int depth, ...){
	va_list ap;
	va_start(ap, depth);
	for(long i = 0; i < depth; i++) {
		long Id = va_arg(ap, int);
		tree = get_tree_child(tree, Id);
	}
	va_end(ap);
	return tree;
}

int add_tree_child(Tree* parent, Tree* child){
	if(!parent || !child) return 0;
	if((parent->child)){
		Tree* last_child = parent->child;
		while(last_child->next) last_child = last_child->next;
		last_child->next = child;
	} else
		parent->child = child;
	child->parent = parent;
	return 1;
}

int main(int argc, char **argv){
	Tree* root;
	int i2 = 2, i3 = 3, i4 = 4;
	root = create_tree(Igd, &i2, IntType, 0);
	root->associatedTokens = tokenList;
	Tree *parent = NULL;
	Tree *newChild = NULL;

	parent = root;
	newChild = create_tree(Ass, &i3, IntType, 0);
	newChild->get = &dummy_get;
	newChild->set = &dummy_set;
	newChild->associatedTokens = tokenList;
	add_tree_child(parent, newChild);
	newChild = create_tree(Summary, "Summary1", StringType, 0);
	newChild->get = &dummy_get;
	newChild->set = &dummy_set;
	newChild->associatedTokens = tokenList;
	add_tree_child(parent, newChild);
	parent = get_tree_recursive(root, 1, Summary);
	newChild = create_tree(Summary, "Summary2", StringType, 0);
	newChild->get = &dummy_get;
	newChild->set = &dummy_set;
	newChild->associatedTokens = tokenList;
	add_tree_child(parent, newChild);

	get_tree_value(get_tree_recursive(root, 1, Ass), NULL);
	get_tree_value(get_tree_recursive(root, 1, Summary), NULL);
	get_tree_value(get_tree_recursive(root, 2, Summary, Summary), NULL);
	delete_tree(root);
	return 0;
}