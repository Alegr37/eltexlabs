#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>

#define true 1
#define false 1
#define maxRedneckCount 10
#define goldPerRedneck 33
#define maxSleepTime 10

int main(){
    int gold = 1100;
    int redneckCount = 0;
    printf("Всего %d золота\n", gold);
    srand(time(NULL)); // seed работает нормально, rand() вызывается в родительском процессе
    while(true){
        if(gold > goldPerRedneck*redneckCount && redneckCount < maxRedneckCount){   //Гномы отправляются в шахту только если есть золото, не занятое добычей
//        if(gold > 0 && redneckCount < maxRedneckCount){                                        //Альтернативный вариант ^
            ++redneckCount;
            int sleepTime = rand()%maxSleepTime;
            pid_t pid = fork();
            if(pid == 0){
                printf("Гном #%d начал работу (%d сек.)\n", getpid(),  sleepTime);
                fflush(stdout);
                sleep(sleepTime);
                return goldPerRedneck;
            }
        }
        else{
            int status, goldMined;
            pid_t closedProcess = wait(&status);
            goldMined = WEXITSTATUS(status);
            if(goldMined > gold) goldMined = gold;
            gold-=goldMined;
            printf("Гном #%d закончил работу (В шахте %d) (%d зол.)\n\t\tОсталось %d золота\n", closedProcess, redneckCount, goldMined, gold);
            fflush(stdout);
            --redneckCount;
        }
        if (!gold){
            printf("\tЗолото добыто, все свободны :)\n");
            break;
        }
    }
    return 0;
}
