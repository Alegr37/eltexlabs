#include <sys/types.h>
#include <sys/msg.h>
#include <stdio.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
/*
    Бригадный подряд.
    Родительский процесс создает заданное количество дочерних (условных рабочих), которые выкладывают на конвейер (в файл, глобальный массив) деталь
    (например, свой идентификатор) если на нем есть место. После запуска рабочих, запускается дочерний процесс-контролер, который снимает с конвейера
    деталь для проверки (есть ли такой идентификатор в списке процессов). Если рабочий не может выложить деталь за число тактов, равное числу рабочих
    (нет места на конвейере), он прерывает работу с жалобой на контролера. Если контролер не может взять с конвейера ни одной детали, он прерывает
    работу с жалобой на бригаду.
*/

// Количество рабочих
#define workerCount 10

// Инициализация очереди
int mqid = -1;

// Описание структуры сообщения
struct myMsgBuff{
    long mtype;
    pid_t somePid;
};

// Функция записи в очередь (Line - конвейер*)
int putOnLine(int tryNumber, pid_t somePid){
    int putSuccess = 0;
    struct myMsgBuff msg = {.mtype=somePid, .somePid=somePid};
    struct msqid_ds status;
    msgctl(mqid, IPC_STAT, &status);
    if(status.msg_qnum < workerCount){
        putSuccess = (msgsnd(mqid, (void *)&msg, sizeof(msg.somePid), IPC_NOWAIT) == 0);
        if(putSuccess)
            printf(">>> Рабочий установил деталь #%d (Попытка №%d)\n", somePid, tryNumber);
        else{
            printf("XXX Рабочий не установил деталь #%d (Попытка №%d, Ошибка %s)\n", somePid, tryNumber, strerror(errno));
        }
    } else{
        printf("XXX Рабочий не установил деталь #%d (Попытка №%d, Нет места)\n", somePid, tryNumber);
    }


    fflush(stdout);
    return putSuccess;
}

// Функция чтения из очереди
int getFromLine(){
    struct myMsgBuff msg;
    int getSuccess = (msgrcv(mqid, (void *)&msg, sizeof(msg.somePid), 0, IPC_NOWAIT) == sizeof(msg.somePid));
    if(getSuccess)
        printf("<<< Контролёр извлёк деталь #%d\n", msg.somePid);
    else
        printf("XXX Контролёр не нашёл деталей, рабочие мудаки\n");
    fflush(stdout);
    return getSuccess;
}


int main(){
    if((mqid = msgget(ftok(".", 0), 0666 | IPC_CREAT)) == -1){
        printf("Ошибка создания очереди: %s", strerror(errno));
        exit(-2);
    }
    pid_t chldPid[workerCount];
    for(int i = 0; i < workerCount; ++i){
        if((chldPid[i] = fork()) == 0){
            // количество попыток расположения детали
            int tryNumber = 0;

            // цикл рабочего
            while(1){
                if(!putOnLine(++tryNumber, getpid())) {
                    if(tryNumber > workerCount){
                        printf("Рабочий #%d не смог установить деталь за %d попыток, контроллёр мудак\n", getpid(), tryNumber);
                        fflush(stdout);
                        break;
                    }
                }
                else tryNumber = 0;
                sleep(1);
            }

            exit(0);
        }
    }

    // цикл контроллера
    do {
        sleep(1);
    } while (getFromLine());

    // Избежание зомбей
    int status;
    for(int i = 0; i < workerCount; ++i) wait(&status);

    // удаление очереди
    msgctl(mqid, IPC_RMID, NULL);
    return 0;
}
