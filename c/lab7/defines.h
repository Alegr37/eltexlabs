#ifndef DEFINES_H
#define DEFINES_H


#define true 1
#define false 0
#define mapWidth 10
#define mapHeight 5
#define firstPlayer 2
#define maxPlayers 4


#define clearColorId -2
#define trapColorId -1
#define grassColorId 0
#define healColorId 1
#define playerColorId 2
#define visitedColorId 3
#define finishColorId 4
#define deathColorId 5

#endif // DEFINES_H
