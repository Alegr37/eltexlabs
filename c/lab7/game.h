#include <stdlib.h>
#include <stdio.h>
#include "defines.h"

struct Player{
    int x, y, toX, toY;
    int achived;
    int lives;
    int sleepTime;
};

struct Player * createPlayers(int *map, int playersCount);
void freePlayers(struct Player * players);
void setNewCoords(struct Player* player);
int *createMap();
int *createColorIdMap(int *map);
void printColor(int i);
void freeMap(int* map);
void printMap(int* map, struct Player* players, int* colorMap);
void moveToNewCoords(int* map, struct Player* player);
