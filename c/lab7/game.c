#include "game.h"

/* ОГЛЯДЫВАЯСЬ НАЗАД, ДУМАЮ СТОИЛО ТАКИ ПРИКРУТИТЬ ncurses, ВЕЛОСИПЕДОВ С ОТОБРАЖЕНИЕМ СТАЛО БЫ МЕНЬШЕ */
/* В следующей лабе так и сделаю */
/* Выделение памяти под игроков */
struct Player * createPlayers(int* map, int playersCount){
    struct Player* players = malloc(sizeof(struct Player)*playersCount);
    for(int i = 0; i < playersCount; ++i){
        struct Player chldPlayer = {.x = rand()%mapWidth, .y = rand()%mapHeight, .toX = rand()%mapWidth, .toY = rand()%mapHeight, .lives = 3};
        chldPlayer.achived = (chldPlayer.x == chldPlayer.toX && chldPlayer.y == chldPlayer.toY);
        map[chldPlayer.y*mapWidth+chldPlayer.x]=0;
        players[i] = chldPlayer;
    }
    return players;
}

/* Освобождение памяти из под игроков */
void freePlayers(struct Player * players){
    free(players);
}

/* Установить игроку новые координаты (в направлении к цели) */
void setNewCoords(struct Player* player){
    if (player->x != player->toX){
        if ((player->toX - player->x) > 0){
            player->x++;
        }
        else{
            player->x--;
        }
    }
    else {
        if (player->y != player->toY){
            if ((player->toY - player->y) > 0){
                player->y++;
            }
            else{
                player->y--;
            }
        }
    }
}

/* Ходим по установленным координатам */
void moveToNewCoords(int* map, struct Player* player){
    if(map[player->y*mapWidth+player->x] < 2){
        player->lives += map[player->y*mapWidth+player->x];
        if(player->lives <= 0) player->sleepTime = 0;
    }
    map[player->y*mapWidth+player->x] = 0;
    if(player->y == player->toY && player->x == player->toX) {
        player->achived = true;
        player->sleepTime = 0;
    }
}

/* Создание карты */
int *createMap(){
    int* map = malloc(sizeof(int)*mapWidth*mapHeight);
    for(int i = 0; i < mapWidth*mapHeight; ++i) map[i] = rand()%3-1;
    return map;
}

/* Создание цветовой карты */
int *createColorIdMap(int* map){
    int* colorMap = malloc(sizeof(int)*mapWidth*mapHeight);
    for(int i = 0; i < mapWidth*mapHeight; ++i) colorMap[i] = map[i];
    return colorMap;
}

/* Освобождение карт */
void freeMap(int* map){
    free(map);
}

/* Отображение карты */
void printMap(int* map, struct Player* players,int *colorMap){
    system("clear");
    for(int i = 0; i < maxPlayers; ++i){
        map[players[i].y*mapWidth+players[i].x] = i+firstPlayer;
        if(!players[i].achived){
            if(players[i].lives > 0)
                colorMap[players[i].y*mapWidth+players[i].x] = playerColorId;
            else
                colorMap[players[i].y*mapWidth+players[i].x] = deathColorId;
        }
        else
            colorMap[players[i].y*mapWidth+players[i].x] = finishColorId;
        printf("#%d(sleep: %d; HP %d)) %d %d -> %d %d\n", i, players[i].sleepTime, players[i].lives, players[i].x+1, players[i].y+1, players[i].toX+1, players[i].toY+1);
    }
    for(int i = 0; i < mapHeight; ++i){
        for(int j = 0; j < mapWidth; ++j){
            printColor(colorMap[i*mapWidth+j]);

            if(map[i*mapWidth+j] < 2)
                printf("%d\t", map[i*mapWidth+j]);
            else
                printf("#%d\t", map[i*mapWidth+j]-firstPlayer);

            printColor(clearColorId);
        }
        puts("");
    }

    puts("\n");

    for(int i = 0; i < maxPlayers; ++i){
        if(!players[i].achived ) colorMap[players[i].y*mapWidth+players[i].x] = visitedColorId;
    }
}

/* Цвета */
void printColor(int i)
{
    switch (i) {
    case clearColorId: // сброс цвета
        printf("\033[00m");
        break;
    case trapColorId: // ловушка
        printf("\033[01;31m");
        break;
    case grassColorId: // дорога/трава
        printf("\033[01;37m");
        break;
    case healColorId: // жизнь
        printf("\033[01;32m");
        break;
    case playerColorId: // персонажи
        printf("\033[01;36m");
        break;
    case visitedColorId: // протоптанная дорога
        printf("\033[00m");
        break;
    case finishColorId: // финиш
        printf("\033[01;33m");
        break;
    case deathColorId: // смерть
        printf("\033[01;40m");
        break;
    default:
        break;
    }
}
