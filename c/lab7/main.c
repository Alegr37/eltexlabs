#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include "game.h"
#include "defines.h"

//    Создается игровое поле в виде матрицы, размерность которой определяет размер поля, содержащей в произвольном соотношении нули, отрицательные (например,-1) и
//    положительные (например, 1) значения. Из произвольных точек поля стартуют несколько игроков (процессов), имеющих одну жизнь, движение которых направлено на
//    произвольный конец поля. Каждый игрок, перемещаясь на соседнюю ячейку, читает ее содержимое, записывает вместо него ноль и добавляет содержимое ячейки к своему
//    количеству жизней. Если количество жизней равно нулю, то игрок погибает. Если ячейка занята другим игроком, происходит бой, в котором побеждает игрок с бо̀льшим
//    количеством жизней.


int main(){
    srand(time(NULL));
    pid_t procPid[maxPlayers];

    int pipe1[maxPlayers][2];

    char    writeBuffer[maxPlayers][80];
    char    readBuffer[maxPlayers][80];

    for(int i = 0; i < maxPlayers; ++i) procPid[i] = 0;

    int* map = createMap();
    int* colorMap = createColorIdMap(map);
    struct Player* players = createPlayers(map, maxPlayers);
    printMap(map, players, colorMap);

    int allPlayersAchevedPoint; // все уже добрались до точки назначения (ну или хотябы умерли); используется ниже в do{ }while(...);

    do{
        for (int i = 0; i < maxPlayers; ++i){
            if(!players[i].achived              // форкаем только если ещё не дошёл, живой и procPid[i] ещё не запущен
                    && players[i].lives>0
                    && procPid[i] == 0)
            {
                if(pipe(pipe1[i])==-1) exit(-2);
                players[i].sleepTime = 1;   // время сна, можно и рандомить
                procPid[i] = fork();        // procPid[i] запущен
                if(procPid[i] == 0){
                    sleep(players[i].sleepTime);    //дочерний спит
                    close(pipe1[i][0]);             // закрывает запись пайпа
                    setNewCoords(&players[i]);      //устанавливает новые координаты
                    sprintf(writeBuffer[i], "%d %d", players[i].x,  players[i].y); //подготавливает строку с новыми координатами
                    write(pipe1[i][1], writeBuffer[i], (strlen(writeBuffer[i])+1));//и херачит её в пайп
                    exit(0);                        // выход (по идее exit освобождает ресурсы, и блокировка пайпа снимается,
                                                    //но хз, всё равно блокировка не так важна в данном случае)
                } else {
                    close(pipe1[i][1]);             // если процесс родительский блокируем запись в пайп из родительского процесса
                }

            }
        }

        allPlayersAchevedPoint = 1;
        for (int i = 0; i < maxPlayers; ++i){
            int status;
            waitpid(procPid[i], &status, WNOHANG); // проверяем дочерние процессы, не ждём их
            if(WIFEXITED(status) != 0) {            // если дочерний процесс завершился
                map[players[i].y*mapWidth+players[i].x]=0;  // занимаемую ранее клетку приравняем к 0 (для корректного отображения)
                read(pipe1[i][0], readBuffer[i], sizeof(readBuffer[i])); //читаем из пайпа новые координаты
                sscanf(readBuffer[i], "%d%d", &players[i].x, &players[i].y); // и устанавливаем в координаты игрока

                moveToNewCoords(map, &players[i]);  // ходим (обрабатывается логика жизней и т.д.)
                printMap(map, players, colorMap);   // выводим карту на экран (внутри можно отключить очистку экрана)
                procPid[i] = 0;                     // процесс i уже не запущен
            };
            if(!players[i].achived && players[i].lives>0) { // проверка что все дошли после каждого хода из дочернего прцесса
                allPlayersAchevedPoint = 0;
            }
        }
    } while(!allPlayersAchevedPoint);

    freeMap(map);
    freeMap(colorMap);
    freePlayers(players);
    return 0;
}
