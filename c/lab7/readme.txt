ЛАБОРАТОРНАЯ РАБОТА №7
МЕЖПРОЦЕССНЫЕ КОММУНИКАЦИИ В LINUX. КАНАЛЫ.

1. ЦЕЛЬ РАБОТЫ: 
Изучить способы и средства обмена информацией между процессами средствами неименованных или именованных каналов в программах на языке C в операционных системах семейства Linux.

2. КРАТКИЕ ТЕОРЕТИЧЕСКИЕ СВЕДЕНИЯ.
Программные (неименованные) каналы не имеют имен, и их главным недостатком является невозможность передачи информации между не родственными процессами. Канал создается вызовом piре и предоставляет возможность однонаправленной (односторонней) передачи данных:
	#include <unistd.h> 
	int pipe(int fd[2]); 
и возвращает 0 в случае успешного завершения, -1 - в случае ошибки. Функция возвращает два файловых дескриптора: fd[0] и fd[1], причем первый открыт для чтения, а второй — для записи.
При необходимости передачи данных в обе стороны нужно создавать пару каналов и использовать каждый из них для передачи данных в одну сторону. Этапы создания двунаправленного канала IPC следующие:
Создаются каналы 1 (fdl[0] и fdl[l]) и 2 (fd2[0] и fd2[l]).
Производится вызов fork.
Родительский процесс закрывает доступный для чтения конец канала 1 (fdl [0] ) и доступный для записи конец канала 2 (fd2[l]).
Дочерний процесс закрывает доступный для записи конец канала 1 (fd1[1]) и доступный для чтения конец канала 2 (fd2[0]).
Текст программы mainpipe.c доступен на сайте в папке «Pipes».
Другим примером использования неименованных каналов является имеющаяся в стандартной библиотеке ввода-вывода функция рореn, которая создает канал и запускает другой процесс, записывающий данные в этот канал или считывающий их из него:
	#include <stdio.h>
	FILE *popen(const char *command, const char *type);
	int pclose(FILE *stream);
Аргумент command представляет собой команду интерпретатора. Он обрабатывается программой sh (интерпретатор Bourne shell), поэтому для поиска исполняемого файла, вызываемого командой command, используется переменная PATH. Канал создается между вызывающим процессом и указанной командой. Возвращаемое функцией рореn значение представляет собой NULL - в случае ошибки или обычный указатель на тип FILE, который может использоваться для ввода или для вывода в зависимости от содержимого строки type:
если type имеет значение r, вызывающий процесс считывает данные, направляемые командой command в стандартный поток вывода;
если type имеет значение w, вызывающий процесс записывает данные в стандартный поток ввода команды command.
Функция pclose закрывает стандартный поток ввода-вывода stream, созданный командой рореn, ждет завершения работы программы и возвращает код завершения, принимаемый от интерпретатора или -1 - в случае ошибки.
Именованные каналы (FIFO) в Unix функционируют подобно неименованным — они позволяют пере­давать данные только в одну сторону. Однако в отличие от программных каналов каждому каналу FIFO сопоставляется полное имя в файловой системе, что позволяет двум неродственным процессам обратиться к одному и тому же FIFO.
FIFO создается функцией mkfifo:
	#include <sys/types.h>
	#include <sys/stat.h>
	int mkfifo(const char *pathname, mode_t mode),
	/* Возвращает 0 при успешном выполнении  -1 - при возникновении ошибок */
После создания канал FIFO должен быть открыт на чтение или запись с по­мощью либо функции open, либо одной из стандартных функций открытия фай­лов из библиотеки ввода-вывода (например, fopen). FIFO может быть открыт либо только на чтение, либо только на запись. Нельзя открывать канал на чтение и за­пись, поскольку именованные каналы могут быть только односторонними. При записи в программный канал или канал FIFO вызовом write данные все­гда добавляются к уже имеющимся, а вызов read считывает данные, помещенные в программный канал или FIFO первыми.
Пример:
	#define FIFO1 "/tmp/fifo.1"
	#define FIFO2 "/tmp/fifo.2"
	#define FILE_MODE (S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH) 
	/* создание двух FIFO, если существуют - OK */
	if ((mkfifo(FIF01, FILE_MODE) < 0) && (errno != EEXIST)) {
		printf("can't create %s", FIFO1); exit(-1); }
	if ((mkfifo(FIFO2, FILE_MODE) < 0) && (errno != EEXIST)) {
		unlink(FIFO1); printf ("can't create %s", FIF02); 
	exit(-2); 
	}
	readfd = open(FIFO1, O_RDONLY, 0);
	writefd = open(FIF02, O_WRONLY, 0);
	write(writefd, buff, len); /* запись в канал IPC */
	/* считывание из канала */
	while ( (n= read(readfd, buff, len)) > 0)
	close(FIF01); close(FIF02);
	unlink(FIFO1); unlink(FIF02);

Заметим, что чтение из FIFO блокирует процесс, если канал еще не открыт на запись каким-либо другим процессом.
Примеры клиент-серверных приложений, использующих неименованные и именованные каналы, доступны по адресу http://gun.cs.nstu.ru/ssw/Pipes.

?===============?
3. МЕТОДИЧЕСКИЕ УКАЗАНИЯ. 
3.1. Для обмена данными между неродственными процессами рекомендуется использовать именованные каналы. Для обмена информацией между родительскими и порожденными процессами рекомендуется использовать неименованные каналы.
3.2. Для ознакомления с функциями работы с каналами используйте инструкции man pipe и man 3 mkfifo, man 2 read (write).
3.3. Для отладки рекомендуется использовать отладчик gdb, информацию о котором можно получить инструкцией man gdb.
3.4. Для отладки и запуска программ, протоколирования их результатов и сохранения на локальном компьютере см. методические указания к лабораторной работе №6.

4. ПОРЯДОК ВЫПОЛНЕНИЯ РАБОТЫ. 
4.1. В соответствии с особенностями реализации варианта задания выбрать средство реализации межзадачных коммуникаций.
4.2. Модифицировать и отладить в соответствии с вариантом и выбранным средством коммуникации программу из лабораторной работы №6, реализующую порожденный процесс. При необходимости модифицировать ее в отдельную программу-сервер.
4.3. Модифицировать и отладить в соответствии с вариантом и выбранным средством коммуникации программу из лабораторной работы №6, реализующую родительский процесс. При необходимости модифицировать ее в отдельную программу-клиент.
?===============?

5. ВАРИАНТЫ ЗАДАНИЙ. 
См. варианты заданий в файле variants.txt.

6. КОНТРОЛЬНЫЕ ВОПРОСЫ. 
1. Что такое каналы? В чем отличие неименованных и именованных каналов?
2. В чем разница при использовании для коммуникаций процессов файлов и каналов?
3. Опишите функции создания и использования неименованных каналов.
4. Чем работа с неименованными каналами отличается от работы с файлами?
5. Опишите особенности использования функции popen().
6. Опишите особенности создания и уничтожения именованных каналов в сравнении с неименованными каналами.
7. Чем работа (чтение/запись) с именованными каналами отличается от работы с неименованными каналами?
8. В каких случаях при работе с неименованными каналами и именованными каналами возникают блокировки в программе?
9. Каковы особенности обхода блокировок при работе с неименованными каналами?
10. Каковы особенности поведения функций работы с каналами при обходе блокировок?

!===============!
СОСТАВИТЕЛИ:
Гунько А.В., канд. техн. наук, доц. НГТУ
Чистяков Н.А., инженер НГТУ
!===============!

!===============!
ССЫЛКИ:
http://gun.cs.nstu.ru/ssw/sysV.zip
http://gun.cs.nstu.ru/ssw/SystemV%20IP
http://gun.cs.nstu.ru/ssw/Pipes/
http://gun.cs.nstu.ru/ssw/pipes.zip
http://gun.cs.nstu.ru/ssw/Linuxprog.zip
http://gun.cs.nstu.ru/ssw/Linuxprog/
http://www.ibm.com/developerworks/ru/library/au-ipc/
http://www.tldp.org/LDP/lpg/node11.html
http://stackoverflow.com/questions/2784500/how-to-send-a-simple-string-between-two-programs-using-pipes

!===============!
