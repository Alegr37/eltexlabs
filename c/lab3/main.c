#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#define maxLength 50

struct Borrower{
    char surname[maxLength];
    char bornTime[maxLength];       //возможно стоит сменить тип на t_time, но нет
    char phoneNumber[maxLength];    //возможно стоит сменить тип на long long, но нет
    int debtSize;
};

int borrowerCmp(const void *p1, const void *p2){
    return ((const struct Borrower *)p2)->debtSize - ((const struct Borrower *)p1)->debtSize;
}

void borrowerPrint(const struct Borrower *borrowerArr, int arrSize){
    for(int i = 0; i < arrSize; ++i){
        printf("Должник №%d:\n", i);
        printf("\tФамилия:\t%s\n", borrowerArr[i].surname);
        printf("\tДата рождения:\t%s\n", borrowerArr[i].bornTime);
        printf("\tТелефон:\t%s\n", borrowerArr[i].phoneNumber);
        printf("\tСумма долга:\t%d\n", borrowerArr[i].debtSize);
    }
}

int main(int argc, char** argv){
    int arrSize = 0;
    printf("Введите количество человек в БД (размерность массива):\n");
    scanf("%d", &arrSize);
    struct Borrower* borrowerArr = malloc(sizeof(struct Borrower)*arrSize);
    if(borrowerArr){
    for(int i = 0; i < arrSize; ++i){
        printf("Заполнение БД (%d из %d)\n", i+1, arrSize);
        printf("Введите фамилию должника: ");
        scanf("%s", &borrowerArr[i].surname);
        printf("Введите дату рождения должника: ");
        scanf("%s", &borrowerArr[i].bornTime);
        printf("Введите телефон должника: ");
        scanf("%s", &borrowerArr[i].phoneNumber);
        printf("Введите сумму долга: ");
        scanf("%d", &borrowerArr[i].debtSize);
    }
    qsort(borrowerArr, arrSize, sizeof(struct Borrower), borrowerCmp);
    borrowerPrint(borrowerArr, arrSize);
    free(borrowerArr);
    exit(EXIT_SUCCESS);
    }
    else {
        exit(EXIT_FAILURE);
    }
}
