#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <pthread.h>
#include <errno.h>
/*
    Контрольная сумма. Для нескольких файлов (разного размера) требуется вычислить контрольную сумму (сумму кодов всех символов файла).
    Обработка каждого файла выполняется в отдельном процессе.
*/
#define handle_error_en(en, msg) \
    do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)

#define handle_error(msg) \
    do { perror(msg); exit(EXIT_FAILURE); } while (0)

#define true 1
#define false 0
static long long globalChecksum = 0;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void addToGlobalChecksum(long long someChecksum){
    // count here
    pthread_mutex_lock(&mutex);
    globalChecksum += someChecksum;
    pthread_mutex_unlock(&mutex);
}

static void *thread_start(void *fileName)
{
    unsigned long long mchecksum = 0;
    FILE* someFile = fopen(fileName, "r");
    if (someFile == NULL)
    {
        fputs("Ошибка файла", stderr);
        exit(1);
    }
    // определяем размер файла
    fseek(someFile , 0 , SEEK_END);                          // устанавливаем позицию в конец файла
    long lSize = ftell(someFile);                            // получаем размер в байтах
    rewind (someFile);                                       // устанавливаем указатель в конец файла
    char * buffer = (char*) malloc(sizeof(char) * lSize); // выделить память для хранения содержимого файла
    if (buffer == NULL)
    {
        fputs("Ошибка памяти", stderr);
        exit(2);
    }
    size_t size = fread(buffer, 1, lSize, someFile);       // считываем файл в буфер
    if (size != lSize)
    {
        fputs("Ошибка чтения", stderr);
        exit (3);
    }
    
    //содержимое файла теперь находится в буфере
    for(int i = 0; i < size; ++i){
        mchecksum+=buffer[i];
    }
    // завершение работы
    fclose (someFile);
    free (buffer);
    printf("Checksum for file \"%s\" = %llu\n", fileName, mchecksum);
    addToGlobalChecksum(mchecksum);
    return fileName;
}

int main(int argc, char** argv){
    int stat;
    void *res;
    if(argc < 2){
        printf("Недостаточно параметров!\nИспользуйте: %s [fileName1] [fileName2] ...", *argv);
        exit(EXIT_FAILURE);
    }
    pthread_t* threads = calloc(argc-1, sizeof(pthread_t));
    for(int i = 0; i < argc-1; ++i){
        stat = pthread_create(&threads[i], NULL,
                              &thread_start, argv[i+1]);
        if (stat != 0)
            handle_error_en(stat, "pthread_create");
    }
    
    for (int i = 0; i < argc-1; i++) {
        stat = pthread_join(threads[i], &res);
        if (stat != 0)
            handle_error_en(stat, "pthread_join");
        printf("Thread for file \"%s\" was joined \n", (char *) res);
    }
    printf("Global checksum for allFiles = %llu\n", globalChecksum);
    free(threads);
    exit(EXIT_SUCCESS);
}
