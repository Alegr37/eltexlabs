Вариант 4 - Грачев А.М.
ФУНКЦИИ, УКАЗАТЕЛИ
ЦЕЛЬ РАБОТЫ: освоить правила написания и использования функций в языке СИ. 
Научиться использовать указатели при обработке массивов данных.

ЗАДАНИЕ 4:
Написать программу сортировки массива строк по вариантам.
Ввод данных, сортировку и вывод результатов оформить в виде функций.
Входные и выходные параметры функции сортировки указаны в варианте. 

Расположить строки в обратном алфавитном порядке 
Входные параметры:
1. Массив 
2. Размерность массива 
Выходные параметры:
1. Количество перестановок
2. Длина первой строки 
