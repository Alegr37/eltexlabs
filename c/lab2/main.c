#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void swap(char** p1, char** p2){
    char* p3 = *p1;
    *p1 = *p2;
    *p2 = p3;
    #ifdef DEBUG
    	printf("swap: %s %s => %s %s\n", *p2, *p1, *p1, *p2);
    #endif
}

int mySort(char** strArr, int strCount){
    int swapCounter = 0;
    int swapNum = 0;
    for (int i = 0; i < strCount; ++i){
        char* str = strArr[i];
        for(int j = i+1; j < strCount; ++j){
            if(strcmp(str, strArr[j]) < 0) {
                swapNum = j;
                str = strArr[j];
            }
        }
        if(swapNum > i) {
            swap(&strArr[i],&strArr[swapNum]);
            ++swapCounter;
        }
    }
    return swapCounter;
}

int main(int argc, char** argv){
    if(argc < 2) {
        fprintf(stderr, "Использование: %s <string>...\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    int swapCount = mySort(argv+1, argc-1);
    printf("%d %d\n", swapCount, strlen(argv[1]));

    #ifdef DEBUG
    	for (int i = 1; i < argc; ++i){
        	puts(argv[i]);
    	}
    #endif
    exit(EXIT_SUCCESS);
    return 0;
}
