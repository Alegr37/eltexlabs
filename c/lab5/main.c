#include <stdio.h>
#include <string.h>
#include <stdlib.h>

extern int myDiv(int a, int b);
extern int myMod(int a, int b);

int main(int argc, char** argv){
    int arg1, arg2;
    char op;
    if (argc < 4){
        fprintf (stderr, "Ошибка! Используйте: %s <arg1> <операция[/\%]> <arg2>\n", argv[0]);
        exit (1);
    }
    arg1 = atoi(argv[1]);
    op = *argv[2];
    arg2 = atoi(argv[3]);
    int res;
    switch (op) {
    case '%':
        res = myMod(arg1, arg2);
        break;
    case '/':
        res = myDiv(arg1, arg2);
        break;
    default:
        printf("%s\n", "неверный оператор");
        exit(-1);
        break;
    }
    printf("%d\n", res);
    return 0;
}
