#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dlfcn.h>

extern int myDiv(int a, int b);
extern int myMod(int a, int b);

int main(int argc, char** argv){
    int arg1, arg2;
    char op;
    if (argc < 4){
        fprintf (stderr, "Ошибка! Используйте: %s <arg1> <операция[/\%]> <arg2>\n", argv[0]);
        exit (1);
    }
    void *ext_library =  dlopen ("libmyOperationDyn.so",RTLD_LAZY);; 
    if (!ext_library)
     {
         fprintf(stderr,"dlopen() error: %s\n", dlerror());
         return 1; 
    }; 
    int (*myMod)(int arg1, int arg2) = dlsym (ext_library,"myMod");
    int (*myDiv)(int arg1, int arg2) = dlsym (ext_library,"myDiv");
    if (!myDiv || !myDiv)
     {
         fprintf(stderr,"dlsym() error: %s\n", dlerror());
         return 1; 
    }; 
    arg1 = atoi(argv[1]);
    op = *argv[2];
    arg2 = atoi(argv[3]);
    int res;
    switch (op) {
    case '%':
        res = (*myMod)(arg1, arg2);
        break;
    case '/':
        res = (*myDiv)(arg1, arg2);
        break;
    default:
        printf("%s\n", "неверный оператор");
        exit(-1);
        break;
    }
    printf("%d\n", res);
    return 0;
}
