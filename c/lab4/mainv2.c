#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define fileType ".cutv2"

int main(int argc, char** argv){
    if (argc < 3 || strlen(argv[2]) > 1){
        fprintf (stderr, "Ошибка! Используйте: %s <имя файла> <символ>\n", argv[0]);
        exit (1);
    }
    FILE* ptrFile = fopen(argv[1], "r");
    if (ptrFile == NULL){
        printf("Не удается открыть файл.\n");
        exit(1);
    };

    // Имя нового файла и его создание
    char* newFileName = malloc(sizeof(char)*strlen(argv[1])+strlen(fileType));
    strcpy(newFileName, argv[1]);
    strcat(newFileName, fileType);
    FILE* ptrNewFile = fopen(newFileName, "w+");
    if (ptrNewFile == NULL){
        printf("Не удается создать файл.\n");
        exit(1);
    };

    // Посимвольное чтение и запись
    char ch;
    int itNewLine = 1;
    while((ch=fgetc(ptrFile)) != EOF) {
        if(itNewLine) {printf(">>"); itNewLine=0;}
        if(ch == '\n') itNewLine = 1;
        if(ch == '+' || ch == '-' ||
                ch == '*' || ch == '/' ||
                ch == '.' || ch == ',' ||
                ch == '?' || ch == '!') ch = argv[2][0];
            printf("%c", ch);
            fputc(ch, ptrNewFile);
    }
    printf("сохранено в %s\n", newFileName);

    // завершение работы, закрытие файлов, освобождение памяти
    free(newFileName);
    fclose (ptrFile);
    fclose (ptrNewFile);

    return 0;
}
