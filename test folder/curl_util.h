#ifndef CURL_UTIL_H
#define CURL_UTIL_H
#include <stdlib.h>

typedef enum {
	CURL_UTIL_OK,
	CURL_UTIL_ERROR,
	CURL_UTIL_ERROR_PROTO,
} curl_util_res;

typedef enum {
	CURL_UTIL_HTTP,
	CURL_UTIL_FTP,
	CURL_UTIL_TFTP,
	CURL_UTIL_ANY,
} curl_util_proto; 


typedef struct {
	char* url;
	const char *filename;
	char verbose;
	char noprogress;
	int timeout;
	curl_util_proto used_proto;
} CURL_opts;

curl_util_res curl_get_file(CURL_opts *curl_opts);
#endif //CURL_UTIL_H