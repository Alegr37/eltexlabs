#include <curl/curl.h>
#include <stdio.h>
#include "curl_util.h"

size_t curl_write_data(void *ptr, size_t size, size_t nmemb, void *stream)
{
	size_t written = fwrite(ptr, size, nmemb, (FILE *)stream);
	return written;
}

curl_util_res curl_get_file(CURL_opts *curl_opts){
	FILE *outfile;
	CURL *curl_handle;
	char urlBuff[512];
	char protoStr[15];
	char *strstr_res;
	CURLcode performRes;
	int prtall = 0;
	if(curl_opts->used_proto != CURL_UTIL_ANY){
		switch(curl_opts->used_proto){
			case CURL_UTIL_HTTP:{
				strcpy(protoStr, "http://");
			} break;
			case CURL_UTIL_FTP:{
				strcpy(protoStr, "ftp://");
			} break;
			case CURL_UTIL_TFTP:{
				strcpy(protoStr, "tftp://");
			} break;
			default:
			return CURL_UTIL_ERROR_PROTO;
		}

		if(strstr(curl_opts->url, "://") == NULL){
			sprintf(urlBuff, "%s%s", protoStr, curl_opts->url);
			curl_opts->url = urlBuff;
		}
		strstr_res = strstr(curl_opts->url, protoStr);

		if(strstr_res == NULL || (strstr_res - curl_opts->url) > 0){
			return CURL_UTIL_ERROR_PROTO;
		}
	}
	
	curl_handle = curl_easy_init();
	curl_easy_setopt(curl_handle, CURLOPT_TIMEOUT , curl_opts->timeout);
	curl_easy_setopt(curl_handle, CURLOPT_URL, curl_opts->url);
	curl_easy_setopt(curl_handle, CURLOPT_VERBOSE, curl_opts->verbose);
	curl_easy_setopt(curl_handle, CURLOPT_NOPROGRESS, curl_opts->noprogress);
	curl_easy_setopt(curl_handle, CURLOPT_FAILONERROR, 1);
	curl_easy_setopt(curl_handle, CURLOPT_LOCALPORT, CURL_UTIL_LOCALPORT);
	curl_easy_setopt(curl_handle, CURLOPT_LOCALPORTRANGE, CURL_UTIL_LOCALPORTRANGE);
	/* send all data to this function	*/
	curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, curl_write_data);
	outfile = fopen(curl_opts->filename, "wb");
	if(outfile) {
		/* write the page body to this file handle */
		curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, outfile);
		/* get it! */
		performRes = curl_easy_perform(curl_handle);
#ifdef CURL_UTIL_DEBUG
		CURLcode res;
		if(CURLE_OK == performRes) {
			double val;
			res = curl_easy_getinfo(curl_handle, CURLINFO_SIZE_DOWNLOAD, &val);
			if((CURLE_OK == res) && (val>0))
				printf("Data downloaded: %0.0f bytes.\n", val);
			res = curl_easy_getinfo(curl_handle, CURLINFO_TOTAL_TIME, &val);
			if((CURLE_OK == res) && (val>0))
				printf("Total download time: %0.3f sec.\n", val);
			res = curl_easy_getinfo(curl_handle, CURLINFO_SPEED_DOWNLOAD, &val);
			if((CURLE_OK == res) && (val>0))
				printf("Average download speed: %0.3f kbyte/sec.\n", val / 1024);

			if(prtall) {
				res = curl_easy_getinfo(curl_handle, CURLINFO_NAMELOOKUP_TIME, &val);
				if((CURLE_OK == res) && (val>0))
					printf("Name lookup time: %0.3f sec.\n", val);
				res = curl_easy_getinfo(curl_handle, CURLINFO_CONNECT_TIME, &val);
				if((CURLE_OK == res) && (val>0))
					printf("Connect time: %0.3f sec.\n", val);
			}
		}
		else {
			fprintf(stderr, "Error while fetching '%s':\n%s\n",
							curl_opts->url, curl_easy_strerror(res));
		}
#endif // CURL_UTIL_DEBUG
		fclose(outfile);
	}
	curl_easy_cleanup(curl_handle);
	if(CURLE_OK == performRes)
		return CURL_UTIL_OK;
	else
		return CURL_UTIL_ERROR;
}